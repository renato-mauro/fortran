! We need to read data from netcdf files ...
!

subroutine input
 use COMM
 implicit none
 integer :: length
 
 length=len_trim(dls_file)
 if((dls_file(length-1:length)=='nc') .or. (dls_file(length-1:length)=='NC')) then
   print*,'now read in a netcdf file ...'
   call inp_nc
 else
   print*,'now read in an ASCII file ...'
   call inp_txt
 endif
 
return
end subroutine input



subroutine inp_nc
 use COMM
 use netcdf
 implicit none
 integer :: ncid,ID_time,ID_lon,ID_lat,ID_var,len1,len2,len3,len
 integer :: i,j,k,i1,i2,i3,it,ky,month,kth,ii,jj,kk,now
 integer :: ndim(2),indx(1)
 real :: miss
 character*(250)  :: filen,title,cname,attribute
! real,allocatable :: value2(:,:)
 real,allocatable  :: dlsss(:,:)
 character*3,dimension(2) :: dim_name  !,cvar
 logical :: exists
 data dim_name/'lon','lat'/   ! They must be in correct order

    call err_handle(nf90_open(dls_file,NF90_nowrite,NCID), 'open dls_file')
    call err_handle(NF90_INQUIRE(NCID,i1,i2),'inqure dim and var numbers')
    if(i1 /=2) stop 'Error: wrong dimension numbers !'
!    call err_handle(NF90_INQ_NVARS(NCID(i),i1),'inq var number')
    if(i2 <3) stop 'Error: wrong variable numbers !'
    N_month=i2-2
   if(N_month /=1 .and. N_month/=13) print*,'The number of variables might be wrong !'

   call err_handle(nf90_get_att(ncid, nf90_global, 'mskfile', mskfile),'get global att -- mskfile')

! get 2 coordinates (dimensions)
    if(NF90_INQ_DIMID (NCID, dim_name(1), ID_lon)/=nf90_noerr)  goto 101
    if(NF90_INQ_DIMID (NCID, dim_name(2), ID_lat)/=nf90_noerr)  goto 101
    call err_handle(NF90_INQUIRE_DIMENSION (NCID,ID_lon, len=Nlon),'inq dim_len for "lon"')
    call err_handle(NF90_INQUIRE_DIMENSION (NCID,ID_lat, len=Nlat),'inq dim_len for "lat"')

   allocate(lon(Nlon),lat(Nlat),dlss(Nlat,0:13),dlsss(Nlon,Nlat))

  do i=1,Nlat
    dlss(i,0)=lat(i)
  enddo

! get the two dimension variables - lon and lat
    call err_handle(NF90_INQ_VARID (NCID, dim_name(1), i1),'inq var ID for lon ')
    call err_handle(NF90_get_var(ncid,i1,lon),'get Var real for lon')
    call err_handle(NF90_INQ_VARID (NCID, dim_name(2), i1),'inq var ID for lat ')
    call err_handle(NF90_get_var(ncid,i1,lat),'get Var real for lat')

! get data Var ...
    do i=0,12
      if(NF90_INQ_VARID (NCID, monthnames(i), i1)==nf90_noerr) then
         call err_handle(NF90_get_var(ncid,i1,dlsss),'get Var real')
         if(NF90_GET_ATT(NCID, i1,'missing_value',miss) == NF90_noerr) then
           where(dlsss == miss) dlsss=mdi
         endif
         if(NF90_GET_ATT(NCID, i1,'_FillValue',miss) == NF90_noerr) then
           where(dlsss == miss) dlsss=mdi
         endif
         dlss(:,i+1)=dlsss(1,:)
       endif
     enddo
     call err_handle(NF90_CLOSE(NCID),'close nc file')

     deallocate(dlsss)

return

101   print*,'Error in subroutine inp_nc:'
   print*,'The dimension names MUST be ',(dim_name(i),i=1,2)
   print*,'Please change the nc data file OR the "dim_name" in this subroutine and try again.'
  stop 'now the code stops....'

end subroutine inp_nc
