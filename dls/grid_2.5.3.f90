! version 2.5.3  by Hongang Yang @ 2011.10.14
! Output result as ASCII and/or netcdf file,
! controlled by the makefile ...
! updated on 09/12/2011

Module COMM
 implicit none
 real,parameter    :: mdi=-99.9        ! missing data index
 integer :: nstats, &  ! number of stations in statfile
            maxny,  &  ! the maximum number of years that will be considered
            nlon,   &  ! values for nlon and nlat need to be reconsidered when dealing with global data
            nlat, &
            Mon_cal(2), & ! The start and end of indices to be calculated, between 1 and 13, can be any order..
            Nmonth,N_month
 character*3   :: monthnames(0:12)
 character*350 :: out_file,mskfile,dls_file
 real,allocatable,save    :: lat(:),lon(:), gridval(:,:,:,:), numbers(:,:,:,:),dlss(:,:)
 integer,allocatable,save :: simvalyr(:),time(:)
 data monthnames/'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Ann'/
end module COMM



program calc_grid_vals_all
 use COMM
 implicit none
 integer,parameter :: zeroobs=0  !set zero vals for output
 character*250 :: statfile,ind,sind,dir,runname
 character*250,allocatable :: statnums(:),indx_file(:)

 character*350 :: opt_dir,indx_dir,ctmp,ctmp2,junk
 integer :: ID0,ID1,ID2,i,i0,ii,jj,kk,ll,k,n,reqmon,ct1,ct2,dls,N_elements,ct,m,Nloc,Ia,Ja,iL, &
            N_lat,indx0(1), tarr(8),thisyr,ifrom,ito, status
 real    :: latlondist,totalfunk,tot,thi,thk, rtemp,data(0:13),data2(0:1)
 logical :: ann_only,exists
 real,allocatable    :: funk(:),reqlons(:),reqlats(:), reqdists(:),newdata(:,:,:),funi(:), &
           wfun(:),simval(:,:),stweights(:),lt1(:),ln1(:),llat(:),llon(:),funi_all(:),funk_all(:,:)
 integer,allocatable :: checkmiss(:),reqstats(:),indx(:)
 double precision    :: tfrom,tto
 integer :: OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM,IDcpu

 namelist /paras/ m,statfile,opt_dir,sind,indx_dir,maxny,Mon_cal,dls_file,runname


!$OMP parallel private(IDcpu);  IDcpu=omp_get_thread_num();   print('(a,i2,a,i2)'),'OpenMP ID # ',IDcpu,' of ',omp_get_num_threads()
!$OMP end parallel

 ID0=10
 call getID(ID0)
 open(ID0,file='grid_2.5.3.nml',status='OLD',IOstat=status)
 if(status/=0) stop 'Error: no input file "grid_2.5.3.nml" ..'
 read(ID0,nml=paras)
 close(ID0)

 call Date_and_Time(values=tarr)
 thisyr=tarr(1)

 call get_time(0,0,tfrom)

 ind='_'//trim(sind)
 dir=trim(sind)//'/' ! for input data files...

 inquire(file=dls_file,Exist=exists)
 if(.Not.exists) then
    print*,'the following input "dls_file" doesn"t exist:'
    print*,trim(dls_file)
    stop 'now code stops !'
 endif

iL=len_trim(mskfile)-4
if(iL<1) stop 'Error: the "mskfile" name is wrong !'

 call getID(ID0)
 inquire(file=statfile,Exist=exists)
 if(.Not.exists) then
  print*, 'Error: the following "statfile" does NOT exist !'
  print*,trim(statfile)
  stop
 endif
 open(ID0,file=statfile)
 nstats=0
 do
   read(ID0,*,end=102) ctmp
   nstats=nstats+1
 enddo
 102 continue
 rewind(ID0)
  
! check parameters ...
if(m>10 .or. m<1) stop 'Error: integer m out of range [0,10] ..'
if(nstats<10)     stop 'Error: nstats too small ...'
if(maxny<10)      stop 'Error: maxny too small ...'
ifrom=minval(Mon_cal)
ito=maxval(Mon_cal)
if(ifrom<1 .or. ito>13) stop 'Error: Mon_cal out of range [1,13]'

allocate(newdata(0:13,0:maxny-1,0:nstats-1),statnums(0:nstats-1),indx_file(0:nstats-1), &  !,Nrecord(0:nstats-1)
        simvalyr(0:maxny-1),llat(0:nstats-1),llon(0:nstats-1),time(0:maxny-1)) !, &
 do i=0,maxny-1
  simvalyr(i)=i+thisyr-maxny+1   !number of years to calculate
  time(i)=simvalyr(i)*10000+101
enddo

newdata=mdi
do i=0,nstats-1  ! to read all the station index data into newdata
  !   read(ID0,'(A11,F13.4,F13.3)') statnums(i),llat(i),llon(i)
   read(ID0,*) statnums(i),llat(i),llon(i)
   ! index file: [station_id]_[index].txt
   indx_file(i)=trim(indx_dir)//trim(dir)//trim(statnums(i))//trim(ind)//'.txt'
enddo
 close(ID0)

 call input
!print*,trim(mskfile)
! print*,''
 print*,'finish reading dls values'
 print*,''

IDcpu=0
ann_only=.false.  ! check if there's only annual value in the data..
ID2=10+IDcpu
call getID(ID2)
inquire(file=indx_file(0),Exist=exists)
if(.Not.exists) then
   print*,trim(indx_file(0))
   stop 'Error: the above station data file does NOT exist'
endif
open(ID2,file=indx_file(0))
read(ID2,'(a50)') junk
if(len_trim(adjustl(junk)) < 20) then
  print*,'There"s ONLY annual value available ...'
  print*,''
  ann_only=.true.
  ifrom=13
  ito=13
endif
 close(ID2)

Mon_cal=(/ifrom,ito/)
Nmonth=ito-ifrom+1

!$OMP parallel default(shared) private(i,ID2,status,junk,data,ct,exists,data2,IDcpu); IDcpu=OMP_GET_THREAD_NUM()
!$OMP do
do i=0,nstats-1
    ID2=10+IDcpu
   call getID(ID2)
   inquire(file=indx_file(i),Exist=exists)
   if(.Not.exists) then
      print*,trim(indx_file(i))
      stop 'Error: the above station data file does NOT exist'
   endif
   open(ID2,file=indx_file(i))
   read(ID2,*) junk
   if(ann_only) then
     do
        read(ID2,*,end=101) data2
        if (data2(0) .ge. simvalyr(0)) then
          ct=data2(0)-(thisyr-maxny+1)
          newdata(0,ct,i)=data2(0)
          newdata(1:13,ct,i)=data2(1)
        endif
      enddo
    else
      do
        read(ID2,*,end=101) data
        if (data(0) .ge. simvalyr(0)) then
          ct=data(0)-(thisyr-maxny+1)
          newdata(:,ct,i)=data
        endif
      enddo
    endif
 101 close(ID2)
enddo
!$OMP end do
!$OMP end parallel
print('(a,i5,a)'),'finish reading all ',nstats,' station index data ...'
print*,''
deallocate(statnums,indx_file)

 allocate(gridval(0:Nlon-1,0:Nlat-1,0:maxny-1,Nmonth),numbers(0:Nlon-1,0:Nlat-1,0:maxny-1,Nmonth))

 write(ctmp,13) simvalyr(0),simvalyr(maxny-1)
13 format(i4.4,'-',i4.4)

write(ctmp2,12)m
12 format(i2)

  out_file=trim(opt_dir)//trim(runname)//trim(ind)//'_'//trim(ctmp)//'_'//trim(mskfile)//'_m'//trim(adjustl(ctmp2))
! after out_file, we can add .txt or .nc or _num.nc for different output format...

 gridval=mdi   ! initialize all the values
 numbers=zeroobs

IDcpu=0
!loop through months (including Ann) ! OpenMP only works with more reqmon        ! H.Yang

!$OMP parallel default(private) shared(monthnames,Nlat,Nlon,lon,lat,dlss,ctmp,ind,mskfile,Nloc,nstats,ln1,lt1,llon,llat, &
!$OMP      gridval,numbers,maxny,newdata,m,simvalyr,Mon_cal) &
!$OMP private(reqstats,reqlons,reqlats,reqdists,ct2,k,i,funi,funi_all,funk_all,simval) ; IDcpu=omp_get_thread_num()
 allocate(reqstats(0:nstats-1),reqlons(0:nstats-1),reqlats(0:nstats-1), reqdists(0:nstats-1))
!$OMP do
do reqmon=Mon_cal(1),Mon_cal(2)
  iL=reqmon-Mon_cal(1)+1
  print*,'Now is for ',monthnames(reqmon-1)

  ct1=0  ! ct1 is i actually.

 do Ia=0,Nlat-1
   dls=dlss(Ia+1,reqmon)

   do Ja=0,Nlon-1
    ct2=0
!set up dummy grid values for each grid, not necessary, it's controlled by ct2 ! H.Yang
	
    do k=0,nstats-1  ! find neighbouring stations
      if(maxval(newdata(reqmon,:,k)) > mdi) then  ! exclude missing index
        call map_2points(lon(Ja+1),lat(Ia+1),llon(k),llat(k),latlondist,rtemp)  ! latlondist in unit of meter.

        if (int(latlondist*0.001) .lt. dls) then  ! "int" was "fix" in IDL
          reqlons(ct2)=llon(k)
          reqstats(ct2)=k
          reqlats(ct2)=llat(k)
          reqdists(ct2)=int(latlondist*0.001)
          ct2=ct2+1		
        endif
      endif
    enddo  ! k loop
    if (ct2 .gt. 2) then     ! at least three neighbouring points
      allocate(funi_all(0:ct2-1),funk_all(0:ct2-1,0:ct2-1),simval(0:ct2-1,0:maxny-1))
      funi_all=(exp(-reqdists(0:ct2-1)/dls))**m                   !correlation function
      funk_all=0.0
      do kk=0,ct2-1
        call map_2points(lon(Ja+1),lat(Ia+1),reqlons(kk),reqlats(kk),rtemp,thi)
        do ll=0,ct2-1
          if (ll .ne. kk) then
            call map_2points(reqlons(ll),reqlats(ll),reqlons(kk),reqlats(kk),rtemp,thk)
            funk_all(kk,ll)=funi_all(ll)*(1.-cos(thk-thi))
          endif
        enddo
      enddo  !kk loop
      simval=mdi  !simulated values for each gridbox

      Do ii=0,ct2-1
        simval(ii,:)=newdata(reqmon,:,reqstats(ii))
      Enddo !ii loop
      Do jj=0,maxny-1
        N_elements=count(simval(:,jj) .ne. mdi)   !check that there are at least 3 neighbouring stations with non-missing data
        if (N_elements .ge. 3) then
          allocate(wfun(0:N_elements-1),funi(0:N_elements-1),funk(0:N_elements-1),checkmiss(0:N_elements-1),stweights(0:N_elements-1))
          n=0
          do i0=0,ct2-1
            if(simval(i0,jj) .ne. mdi) then
              checkmiss(n)=i0
              n=n+1
            endif
          enddo
          funi=funi_all(checkmiss)               !correlation function
          wfun=0.0      !weighting function for each station
          do kk=0,N_elements-1
            totalfunk=sum(funi)-funi(kk)
            funk=funk_all(checkmiss(kk),checkmiss)
            tot=sum(funk)
            wfun(kk)=funi(kk)*(1.+tot/totalfunk)
          enddo  !kk loop
 ! check for divide by zero
          stweights=wfun/sum(wfun) ! the weighting function for each year
          gridval(Ja,Ia,jj,iL)=sum(stweights*simval(checkmiss,jj))
          numbers(Ja,Ia,jj,iL)=N_elements
          deallocate(funi,wfun,checkmiss,stweights,funk)
!        else 
        endif
      enddo  !jj loop

      deallocate(simval,funi_all,funk_all)
    endif
    ct1=ct1+1    ! ct1 is i actually. H.Yang
    if(mod(ct1,500)==0)  print*,ct1,lat(Ia+1),lon(Ja+1),ct2
   enddo  ! Ja
 enddo  ! Ia for locations

  print('(a,i5,a,a)'),'total ',ct1,' locations for ',monthnames(reqmon-1)

enddo ! reqmon loops

!$OMP end do
!$OMP end parallel

  call output
  deallocate(gridval,numbers)

 call get_time(1,0,tto)
 print*,'time usage:',real(tto-tfrom),'seconds'

end program calc_grid_vals_all


