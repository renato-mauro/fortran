! Almost the same as the IDL code, but we assume lon and lat ONLY in unit of degree.
! return distance in unit of meter, and angular distance in radius [-pi,pi]...

subroutine Map_2points(lon0, lat0, lon1, lat1,dis_meter,dis_ang)
implicit none
double precision,parameter :: dpi=3.14159265358979323846264338d0
real :: lon0, lat0, lon1, lat1,dis_meter,dis_ang
double precision :: k,r_earth,cosc,coslt1,coslt0,sinlt1,sinlt0,cosl0l1,sinl0l1,sinc,cosaz,sinaz

 if(abs(lat0) .gt. 90.) stop 'lat0 out of 90 degree !'
 if(abs(lat1) .gt. 90.) stop 'lat1 out of 90 degree !'

k = dpi/180.d0
r_earth = 6378206.4d0 !Earth equatorial radius, in unit of meter, Clarke 1866 ellipsoid

 coslt1 = cos(k*lat1)
 sinlt1 = sin(k*lat1)
 coslt0 = cos(k*lat0)
 sinlt0 = sin(k*lat0)

 cosl0l1 = cos(k*(lon1-lon0))
 sinl0l1 = sin(k*(lon1-lon0))

 cosc = sinlt0 * sinlt1 + coslt0 * coslt1 * cosl0l1 !Cos of angle between two points
if(cosc> 1.d0) cosc=1.d0
if(cosc<-1.d0) cosc=-1.d0
sinc = dsqrt(1.d0 - cosc**2)
! Avoid roundoff problems by clamping cosine range to [-1,1].

if (abs(sinc) .gt. 1.0d-7) then ! Small angle?
    cosaz = (coslt0 * sinlt1 - sinlt0*coslt1*cosl0l1) / sinc !Azimuth
    sinaz = sinl0l1*coslt1/sinc
else		!Its antipodal
    cosaz = 1.d0
    sinaz = 0.d0
endif

dis_meter = acos(cosc)* r_earth
dis_ang   = atan2(sinaz,cosaz)

return
end subroutine Map_2points




! ******* to get a free unit ID for I/O file.
  subroutine getID(ID)
  integer :: ID,ios
  logical :: lopen

!  ID=10
  do 
    inquire(unit=ID,opened=lopen,iostat=ios)
    if(ios==0.and.(.not.lopen)) return
    ID=ID+1
  enddo

  return
  end subroutine getID





 subroutine get_time(Iflag,ID,current)
  integer      :: tarr(8),ii,Iflag, ID
  character*23 :: file
  character*45 :: now(3),printf
  double precision :: current
  data now/'Task starts from:','Task ends at:','Current time:'/

  ii=3
  if(Iflag==0) ii=1
  if(Iflag==1) ii=2
  call Date_and_Time(values=tarr)
  write(file,12) tarr(1),tarr(2),tarr(3),tarr(5),tarr(6),tarr(7),tarr(8)
12  format(i4.4,'.',i2.2,'.',i2.2,' ',i2.2,':',i2.2,':',i2.2,'.',i3.3)

  printf=trim(now(ii))//' '//file

  current= tarr(5)*3600.d0+tarr(6)*60.d0+tarr(7)+0.001d0*tarr(8)

  write(*,'(/,a,/)') printf
  if(ID > 0)  write(ID,'(/,a,/)') printf

  return
  end subroutine get_time 
 