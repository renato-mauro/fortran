+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
This is the version which is compatible with RClimdex and "old" Fclimdex.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. How to install and run FClimdex2
================================
 
compile and run the code (use ifort as example):

1a. on linux: 
 Edit the Makefile, then
    $ make fclimdex.exe
 or $ ifort all.f90 -o fclimdex.exe [-openmp]

to run the code, edit the input.nml, then
    $ ./fclimdex.exe

If OpenMP is used, you can change processor core number # by setting
    $ export OMP_NUM_THREADS=#
    $ ./fclimedx.exe 
by ddefault, it uses all the cores on one machine.

To clean temporary files: 
    $ make clean


1b. on windows machines:
    $ ifort all.f90 -o fclimdex.exe [/Qopenmp]
    $ [ set OMP_NUM_THREADS=# ]
    $ fclimdex.exe


Note:
- the namelist file input.nml must be saved in the run directory (i.e., where fclimdex.exe is executed)
- if there is an error like "segmentation fault", edit the .bashrc file and add one line 'ulimit -s unlimited'.


 
2. Modification history of fclimdex_station version:
====================================

0. change history before 2011.4.11:
  add TMAXmean and TMINmean output in q!  function
  in TN10p subroutine, add an 1e-5 term on all
  thresholds, to eliminate computational error. ( 3.5 may store like
  3.5000001 or 3.49999999 in thresholds )
  changed TN10p subroutine, set missing value for monthly output the
  same level as R, eg. >10 days missing in a month, then set this month
  missing.
   last modified 2008-06-15
  changed percentile funcion to calculate multi-level percentiles in a single
  routine, also changed threshold.

1. use module for all functions: ismiss, nomiss,leapyear.

2. separate code into several files, so it's easy to open and modify one part instead of the whole code.

3. use getID(ID) to set unit # for I/O, in case of I/O confliction.

4. option to compile the code using makefile, will add more compiler option.

5. use a new Mon(12,2) in palce of the original Mon(12) and Monleap(12), reduce the "if else" block, and code is simpler.

6. "TX10p" uses almost all computation resource, so we vectorize part of it, and use OpenMP for parallelization, which scales well.

7. get a new "sort2" subroutine to sort a given array, very simple to code and modify, but it's rather SLOW if the array is VERY big,
   so we continue using the original "sort".

8. get "printMonth" to print the header for some of the output file.

9. What's the use of "Ran2"? It's a random number generator, deleted now.

10. Output reorganised: We put each index (from all stations) in seperate folders.

11. use input file input.nml to control each run.
    So the code only needs to be compiled once on one machine.

12. In CDD.f90, the numbers of CDD and CWD could NOT be updated occasionally, and Markus corrected this.
    In GSL.f90, in one line the index of "ee" could be 0 and cause problem, and Robert corrected this.
    In modules.f90, the old version of PGF90 has problem with "use .. only .."
    also add "-Mbackslash" for pgf90 compiler.
    correct one error in "Rnnmm.f90", now output is correct.

13. modify the TX10P part, to output the thresholds for all days (DoY), which is optional. @ 2011.8.1

14. check if all data are missing, if yes, ignore the calculation/output of relevant indices.

15. add extension '.txt' to the output files

16. corrections in GSL.f90 to allow GSL values up to 365 on Southern hemisphere (previously cut at 183)

17. correction in R95p.f90 to only calculate r95p/r99p if a percentile can be calculated. 
    In some very dry regions the percentile calculation results in MISSING (=-99.9), 
    and r95p/r99p should not be calculated if precipitaion exceeds this value.

18. change the output format @ 2012.6.28:
    years starts from 1st column
    use subroutines to output the indices, except for Heat Wave indices and some usually-unused ones.
    remove the consecutive MISSING years at the beginning and end of the data

19. add ETR, R95pTOT and R99pTOT @ 2012.6.29
    ETR in TXX.f90; Rp5pTOT and R99pTOT in R95p.f90. @ 2012.7.19

20. fix a bug in RX5day.f90, found by Imke @ 2012.7.9
    also reformat the output format

21. add some comments for readers
    check code with definitions @ 2012.7.19
