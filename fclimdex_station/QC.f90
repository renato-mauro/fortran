! --------------------------------------------------------------
!  Quality Contral subroutine
!
!   prepare data set: YMD(3), PRCP, TMAX & TMIN,
!   find all-missing data,
!   and set NASTAT/YNASTAT dataset for missing values monthly/annual.
!
!   (C) Copr. 1986-92 Numerical Recipes Software &#5,.
! --------------------------------------------------------------

      subroutine qc
      use COMM
      use functions
      real(DP),dimension(DoY,3) :: stdval,m1

      character(120) :: title(3)
      integer  :: ios, rno, tmpymd(MaxYear*DoY,3), i,j,ID,IDotmp,IDofil
      integer  :: kth,month,k,trno,ymiss(3),mmiss(3),stdcnt(3),	&
                  missout(MaxYear,13,3),tmpcnt,now,now1, Ky
      real(DP) :: tmpdata(DoY*MaxYear,3),stddata(DoY,MaxYear,3),stdtmp

      data title/"PRCPMISS","TMAXMISS","TMINMISS"/

  ! initialise all checking points
     Tmax_miss=.false.    
     Tmin_miss=.false.
     Prcp_miss=.false.

! open data file
      call getID(ID)
      open(ID, file=trim(data_dir)//trim(ifile), STATUS="OLD", IOSTAT=ios,action='read')
      if(ios.ne.0) then
        write(ID_log,*) "ERROR during opening file: ", trim(ifile)
        write(ID_log,*) "Program STOP!!"
        stop 'ERROR in QC: fail to open ifile !'
      endif

! open QC log files
      call getID(IDotmp)
      open(IDotmp, file=ofile(28))
      write(IDotmp,*) "PRCP Quality Control Log File:"
      call getID(IDofil)
      open(IDofil, file=ofile(29))
      write(IDofil,*) "TMAX and TMIN Quality Control Log File:"

! read original date and data
      rno=1
88    read(ID,*,end=110) (tmpymd(rno,j),j=1,3),	(tmpdata(rno,j),j=1,3)
      rno=rno+1
      goto 88
110   rno=rno-1
      close(ID)

      SYEAR=tmpymd(1,1)
      EYEAR=tmpymd(rno,1)
      YRS=EYEAR-SYEAR+1

! set YMD to be used by all codes
      TOT=0
      do i=SYEAR,EYEAR
        Ky=leapyear(i)+1
        do month=1,12
          Kth=Mon(month,Ky)
          do k=1,kth
            TOT=TOT+1
            YMD(tot,1)=i
            YMD(tot,2)=month
            YMD(tot,3)=k
          enddo
        enddo
      enddo

      PRCP=MISSING
      TMAX=MISSING
      TMIN=MISSING

! insert data in correct place
      j=1
      do i=1, TOT
111     now=YMD(i,1)*10000+YMD(i,2)*100+YMD(i,3)        ! H.Yang
        now1=tmpymd(j,1)*10000+tmpymd(j,2)*100+tmpymd(j,3)
        if(now.eq.now1) then
          PRCP(i)=tmpdata(j,1)
          TMAX(i)=tmpdata(j,2)
          TMIN(i)=tmpdata(j,3)
          j=j+1
        elseif(now.lt.now1) then
          PRCP(i)=MISSING
          TMAX(i)=MISSING
          TMIN(i)=MISSING
        elseif(now.gt.tmpymd(rno,1)*10000+tmpymd(rno,2)*100+tmpymd(rno,3)) then
          PRCP(i)=MISSING
          TMAX(i)=MISSING
          TMIN(i)=MISSING
        else
          j=j+1
          goto 111
        endif
      enddo

! check if data are reasonable, if not, set them as MISSING.
! also find MISSING days for each month and year,
! set NASTAT/YNASTAT dataset for missing values monthly/annual.

      trno=0
      MNASTAT=0
      YNASTAT=0
      do i=SYEAR,EYEAR
        Ky=leapyear(i)+1
        ymiss=0
        stdcnt=0
        do month=1,12
          mmiss=0
          Kth=Mon(month,Ky)
          do k=1,kth
            trno=trno+1
            if(TMAX(trno).le.MISSING) TMAX(trno)=MISSING
            if(TMIN(trno).le.MISSING) TMIN(trno)=MISSING
            if(PRCP(trno).le.MISSING) PRCP(trno)=MISSING
            if(TMAX(trno).lt.TMIN(trno).and.nomiss(TMAX(trno)) &
                        .and.nomiss(TMIN(trno))) then
              TMAX(trno)=MISSING
              TMIN(trno)=MISSING
              write(IDofil, *) i*10000+month*100+k, "TMAX<TMIN!!"
            endif

            if(month.ne.2.or.k.ne.29) then
              stdcnt=stdcnt+1
              stddata(stdcnt, i-SYEAR+1, 1)=PRCP(trno)
              stddata(stdcnt, i-SYEAR+1, 2)=TMAX(trno)
              stddata(stdcnt, i-SYEAR+1, 3)=TMIN(trno)
            endif

            if((TMAX(trno).lt.-70..or.TMAX(trno).gt.70.).and.  &
              nomiss(TMAX(trno))) then
              TMAX(trno)=MISSING
              write(IDofil, *) i*10000+month*100+k, "TMAX over bound!!"
            endif
            if((TMIN(trno).lt.-70..or.TMIN(trno).gt.70.).and.  &
              nomiss(TMIN(trno))) then
              TMIN(trno)=MISSING
              write(IDofil, *) i*10000+month*100+k, "TMIN over bound!!"
            endif
            if(PRCP(trno).lt.0.and.nomiss(PRCP(trno))) then
              PRCP(trno)=MISSING
              write(IDotmp,*) i*10000+month*100+k, "PRCP less then 0!!"
            endif
            if(ismiss(PRCP(trno))) then
              mmiss(1)=mmiss(1)+1
              ymiss(1)=ymiss(1)+1
            endif
            if(ismiss(TMAX(trno))) then
              mmiss(2)=mmiss(2)+1
              ymiss(2)=ymiss(2)+1
            endif
            if(ismiss(TMIN(trno))) then
              mmiss(3)=mmiss(3)+1
              ymiss(3)=ymiss(3)+1
            endif
          enddo  ! day loop

! set NASTAT/YNASTAT dataset for missing values monthly/annual.
          do k=1,3
            missout(i-SYEAR+1,month,k)=mmiss(k)
            if (mmiss(k).gt.3) then
              MNASTAT(i-SYEAR+1,month,k)=1
            endif
          enddo
        enddo   ! month loop

        do k=1,3
          missout(i-SYEAR+1,13,k)=ymiss(k)
          if(ymiss(k).gt.15) then
            YNASTAT(i-SYEAR+1,k)=1
          endif
        enddo
      enddo  ! year loop


! check if all data are MISSING
    if(count(TMAX(1:TOT) .eq. MISSING) .ge. TOT) then
       write(*,'(2(a,i))') "MISSING days: ",count(TMAX(1:TOT) .eq. MISSING),"    TOT days: ", TOT
       print*,"All TMAX are missing, so we will NOT calculate/output Tmax-related indices !"
       Tmax_miss=.true.
    endif
    if(count(TMIN(1:TOT) .eq. MISSING) .ge. TOT) then
       write(*,'(2(a,i))') "MISSING days: ",count(TMIN(1:TOT) .eq. MISSING),"    TOT days: ", TOT
       print*,"All TMIN are missing, so we will NOT calculate/output Tmin-related indices !"
       Tmin_miss=.true.
    endif
    if(count(PRCP(1:TOT) .eq. MISSING) .ge. TOT) then
       write(*,'(2(a,i))') "MISSING days: ",count(PRCP(1:TOT) .eq. MISSING),"    TOT days: ", TOT
       print*,"All PRCP are missing, so we will NOT calculate/output Prcp-related indices !"
       Prcp_miss=.true.
    endif

 ! If all data are MISSING, then we do nothing more...
    if(Tmax_miss .and. Tmin_miss .and. Prcp_miss) return
    
    
!  Calculate STD for PRCP, TMAX and TMIN; then figure out outliers
      stdval=0. 
      m1=0.
      do i=1,DoY
        stdcnt=0
        do j=1,YRS
          do k=2,3
            if(nomiss(stddata(i,j,k))) then
              stdcnt(k)=stdcnt(k)+1
              m1(i,k)=m1(i,k)+stddata(i,j,k)
            endif
          enddo
        enddo
        do k=2,3
          if(stdcnt(k).gt.0) then
            m1(i,k)=m1(i,k)/real(stdcnt(k))
          endif
        enddo
        stdtmp=0.
        do j=1,YRS
          do k=2,3
            if(stdcnt(k).gt.2.and.nomiss(stddata(i,j,k))) then
               stdval(i,k)=stdval(i,k)+			&
              (stddata(i,j,k)-m1(i,k))**2./(real(stdcnt(k))-1.)
            endif
          enddo
        enddo

        do k=2,3
          if(stdcnt(k).gt.2) then
            stdval(i,k)=stdval(i,k)**0.5
          else 
            stdval(i,k)=MISSING
          endif
        enddo
      enddo  ! i loop

! outliers
      trno=0
      do i=SYEAR,EYEAR
        Ky=leapyear(i)+1
        tmpcnt=0
        do month=1,12
          Kth=Mon(month,Ky)
          do k=1, kth
            trno=trno+1
            if(month.ne.2.or.k.ne.29) tmpcnt=tmpcnt+1
            if(nomiss(stdval(tmpcnt,2)))then
              if(abs(TMAX(trno)-m1(tmpcnt,2)).gt.		  &
               stdval(tmpcnt,2)*STDSPAN.and.nomiss(TMAX(trno)))   &
           write(IDofil, *) "Outlier: ", i, month, k, "TMAX: ", TMAX(trno), &
                 "Lower limit:",m1(tmpcnt,2)-stdval(tmpcnt,2)*STDSPAN,	 &
                 "Upper limit:",m1(tmpcnt,2)+stdval(tmpcnt,2)*STDSPAN	 
            endif
            if(nomiss(stdval(tmpcnt,3)))then
              if(abs(TMIN(trno)-m1(tmpcnt,3)).gt.				   &
               stdval(tmpcnt,3)*STDSPAN.and.nomiss(TMIN(trno)))	   &
           write(IDofil, *) "Outlier: ", i, month, k, "TMIN: ", TMIN(trno),	&
                 "Lower limit:",m1(tmpcnt,3)-stdval(tmpcnt,3)*STDSPAN,	&
                 "Upper limit:",m1(tmpcnt,3)+stdval(tmpcnt,3)*STDSPAN	
            endif
          enddo ! end do day
        enddo !end do month
      enddo ! end do year

! output MISSING days
      call getID(ID)
      open(ID, file=ofile(30))
      do i=SYEAR,EYEAR
        do k=1,3
          write(ID,'(i4,2x,a8,13i4)')	 &
         i,title(k),(missout(i+1-SYEAR,j,k),j=1,13)
        enddo
      enddo
      close(ID)
      close(IDotmp)
      close(IDofil)

      return
      end subroutine qc 
