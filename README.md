# fortran

This repo contains the Fortran source code for a number of Climdex-related
utilities.

Note: I'm a newcomer to Fortran and am still learning how everything works, so
these notes are pretty sparse. I hope they're still useful, though!

-- James Goldie, April 2020

## Directory structure:

* **dls:** makes gridding.exe
* **dls_cal:** makes dls.exe

## Things you might need to adapt to your needs

Fortran is a compiled language, and part of these programs' configuration is
controlled by their `Makefile`s, which decides how compilation happens.

In particular, if you're recompiling these programs and you want NetCDF output,
you'll need to define the `NC_LIB` variable according to where the NetCDF
libraries live on your system. Be aware that the NetCDF libraries are also
compiled, and so if you compile on one system and run on another you _may_
hit problems with NetCDF output when running the programs. If you run and get an
error like:

```
./dls.exe: symbol lookup error: ./dls.exe: undefined symbol: netcdf_mp_nf90_create_
```

You can look up which libraries the executable uses using:

```
ldd dls.exe
```

If any report as "not found", you either haven't installed a library or it's in the
wrong location—different systems keep them in different places (`/lib`, `/usr/lib`,
etc.).

If none report as "not found", you might be using an incompatible version. Use this
command to try and find the undefined symbol in the library (I recommend checking
`libnetcdff.so.6`):

```
nm -gD libnetcdff.so.6 | grep netcdf_mp_nf90_create_
```

If it isn't in there (you might try using a more general `grep` term, like `create`,
to see if it has a slightly different name that's mucking things up), you're going to
have to get a different version or try compiling on the same system you run on. Our
current solution is to substitute the NetCDF libraries from the compilation server
(literally copy the shared library files in).

### Input file names

Generally the station input _directory_ is controlled by a _namefile_ (`.nml`) that is
read in at run-time. However, the template for individual station names in hardcoded
into the source and may need to be changed if it doesn't fit your station data (or you
could rename your station files to suit). This leads to errors like:

```
 /datasets/ghcndex/20200304/stn-indices/TXx/AG000060390_TXx
Error: the above station data file does NOT exist
```

Check:

* **dls:** the definition of `indx_file`
* **dls_cal:** the definition of `infile`
